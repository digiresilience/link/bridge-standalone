from flask import Flask, request, jsonify
import requests
import time

app = Flask(__name__)


@app.route('/receive', methods=['POST'])
def receive():
    data = request.get_json()
    url = request.headers.get("X-CDR-Bridge-Send-URL")
    time.sleep(5)
    requests.post(url, json=data)

    return jsonify({'status': 'success'}), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
