# CDR Bridge Standalone

This repository provides a sample Docker Compose file to demonstrate running CDR Bridge as a standalone application, instead of as part of a CDR Link installation.

The Docker images used in this repo are built and maintained in the main [Link Stack repository](https://gitlab.com/digiresilience/link/link-stack).

## Setup

To begin, copy the `.env.sample` file to `.env` and fill in the missing values.

CDR Bridge requires that users be authenticated to use the application. For this simplified deployment, we recommend using Google for authentication. Details on how to create an application in the Google Developer Console can be found on the [NextAuth Google provider configuration page](https://next-auth.js.org/providers/google).

Once you have created the Google application, enter the `GOOGLE_CLIENT_ID` and `GOOGLE_CLIENT_SECRET` values in the `.env` file.

Start the application with the command `docker compose --env-file .env up`. CDR Bridge is under active development, so to make sure you have the latest version, append `--pull always` to the above command: `docker compose --env-file .env up --pull always`.

## API Endpoints

The endpoints for CDR Bridge are:

#### Receive webhooks from an external service

`POST /api/{channelName}/webhooks`

Format varies per service, but it will be formatted as JSON.

#### Send a message

`POST /api/{channelName}/bots/{token}/send`

The body should be JSON in the format:

```
{
  recipient: "phone number or user id (depending on service)",
  message: "Message text"
}
```

#### Receive a message

`POST /api/{channelName}/bots/{token}/receive`

This endpoint is primarily used by the external Signal and WhatsApp containers. The body should be JSON in the format:

```
{
  sender: "phone number or user id (depending on service)",
  message: "Message text"
}
```

#### Get info about a "bot"

`GET /api/{channelName}/bots/{token}`

Returns info about the bot as JSON.

## Configuring Outgoing Webhooks

CDR Bridge is primarily a relay service: messages come into the application from various messaging services and are relayed to other consuming services. This is made possible by associating one or more webhooks with a particular "bot".

When you create a webhook in the CDR Bridge interface, you need to choose which URL the message will be relayed to, what HTTP method to use (PUT or POST), and any additional headers (possibly for authentication) that should be added to the request.

In addition to the user-supplied headers, an additional header `X-CDR-Bridge-Send-URL` will also be added. If consuming services wish to send a reply to the received message, they can use this URL to do so.
